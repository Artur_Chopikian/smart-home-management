from tkinter import *
from tkinter import messagebox
import requests
import re

# ==============================================
# ГЛОБАЛЬНІ ЗМІННІ
# ==============================================
name = ''
surname = ''
email = ''
password = ''
twoPassword = ''
userID = 0
number_list = 0

# ==============================================
# url for requests Flask
# ==============================================
url = ''

# ==============================================
# ологошення головголо вікна tkinter
# ==============================================
root = Tk()
root.resizable(0, 0)


class ConnectToTheDatabase:
    def __init__(self, master):
        self.master = master
        # self.master.title("Підключення до сервера")

        # self.master.resizable(0, 0)
        self.master.winfo_toplevel().title("Підключення до сервера")
        self.master.winfo_toplevel().geometry('300x100')

        center_window(self.master, 300, 100)
        # ==============================================
        # оголошення фреймa
        # ==============================================
        self.connectFrame = Frame()
        self.connectFrame.grid(padx=10, pady=5)
        # ==============================================
        # все що входить до connectFrame
        # ==============================================
        # self.userName = Label(self.connectFrame, width=10, height=2, font=("Helvetica", 13), text="User: ")
        # self.userName.grid(row=0, column=0, sticky="wn", padx=5, pady=5)

        # self.userPassword = Label(self.connectFrame, width=10, height=2, font=("Helvetica", 13), text="Password: ")
        # self.userPassword.grid(row=1, column=0, sticky="wn", padx=5, pady=5)

        self.ipAddressServer = Label(self.connectFrame, width=10, height=2, font=("Helvetica", 13), text="IP сервера: ")
        self.ipAddressServer.grid(row=2, column=0, sticky="wn", padx=5, pady=5)

        # self.inputUserName = Entry(self.connectFrame, width=20, font=("Helvetica", 13))
        # self.inputUserName.grid(row=0, column=1, padx=5, pady=5)

        # self.inputUserPassword = Entry(self.connectFrame, width=20, show="*", font=("Helvetica", 13))
        # self.inputUserPassword.grid(row=1, column=1, padx=5, pady=5)

        self.inputIpAddressServer = Entry(self.connectFrame, width=20, font=("Helvetica", 13))
        self.inputIpAddressServer.grid(row=2, column=1, padx=5, pady=5)

        self.confirmConnect = Button(self.connectFrame, width=15, height=2, text="Підтвердити",
                                     font=("Helvetica", 13),
                                     command=lambda: self.clickConfirmConnect())
        self.confirmConnect.grid(row=3, columnspan=2, padx=5, pady=5)

    def clickConfirmConnect(self, *args):
        global url
        ip_server = {
            'ip_server': self.inputIpAddressServer.get().replace(' ', '')
        }
        if len(self.inputIpAddressServer.get()) != 0:
            try:
                r = requests.get(
                    url='http://' + self.inputIpAddressServer.get().replace(' ', '') + ':5000' + '/connectServer',
                    json=ip_server, timeout=1)
                r.json()

                if int(r.text) == 1:
                    url = 'http://' + str(self.inputIpAddressServer.get().replace(' ', '')) + ':5000'
                    messagebox.showinfo('Повідомлення', "Успішне з'єднання з сервером!")

                    self.connectFrame.destroy()

                    LoginRegister(root)

            except requests.exceptions.RequestException as e:
                messagebox.showinfo('Повідомлення', "Немає з'єднання з сервером!")
        else:
            messagebox.showinfo('Повідомлення', "Поля не можуть бути пусті!")


class LoginRegister:
    message: str

    def __init__(self, master):
        self.master = master
        self.master.winfo_toplevel().title("Авторизація")
        self.master.winfo_toplevel().geometry('500x300')

        center_window(self.master, 500, 300)
        # ==============================================
        # оголошення фреймів
        # ==============================================
        self.loginFrame = Frame(master)
        self.loginFrame.grid(padx=100, pady=50)

        self.registerFrame = Frame(master)
        # ==============================================
        # змінні у яких зберігається інформація про вхід клієнта
        # ==============================================
        self.userLogin = StringVar()
        self.userPassword = StringVar()
        # ==============================================
        # змінні в яких зберається інформація про рейстрацію клієнта
        # ==============================================
        self.registerUserLogin = StringVar()
        self.registerUserName = StringVar()
        self.registerUserSurname = StringVar()
        self.registerUserPassword = StringVar()
        self.registerUserTwoPassword = StringVar()
        # ==============================================
        # все що входить до loginFrame
        # ==============================================
        self.login = Label(self.loginFrame, width=10, height=2, font=("Helvetica", 13), text="Email: ")
        self.login.grid(row=0, column=0, sticky="wn", padx=5, pady=5)

        self.password = Label(self.loginFrame, width=10, height=2, font=("Helvetica", 13), text="Пароль: ")
        self.password.grid(row=1, column=0, sticky="wn", padx=5, pady=5)

        self.inputLogin = Entry(self.loginFrame, width=20, textvariable=self.userLogin, font=("Helvetica", 13))
        self.inputLogin.grid(row=0, column=1, padx=5, pady=5)

        self.inputPassword = Entry(self.loginFrame, width=20, textvariable=self.userPassword, show="*",
                                   font=("Helvetica", 13))
        self.inputPassword.grid(row=1, column=1, padx=5, pady=5)

        self.log_in = Button(self.loginFrame, width=15, height=2, text="Увійти", font=("Helvetica", 13),
                             command=lambda: self.clickLogin())
        self.log_in.grid(row=2, columnspan=2, padx=5, pady=5)

        self.register = Button(self.loginFrame, width=15, height=2, text="Зареєструватися", font=("Helvetica", 13),
                               command=lambda: self.clickRegister())
        self.register.grid(row=3, columnspan=2, padx=5, pady=5)
        # ==============================================
        # все що входить до registerFrame
        # ==============================================

        self.registerName = Label(self.registerFrame, text="Ім'я: ", font=("Helvetica", 13))
        self.registerName.grid(row=0, column=0, sticky="wn", padx=5, pady=5)

        self.registerInputName = Entry(self.registerFrame, textvariable=self.registerUserName, font=("Helvetica", 13))
        self.registerInputName.grid(row=0, column=1, padx=5, pady=5)

        self.registerSurname = Label(self.registerFrame, text="Прізвище: ", font=("Helvetica", 13))
        self.registerSurname.grid(row=1, column=0, sticky="wn", padx=5, pady=5)

        self.registerInputSurname = Entry(self.registerFrame, textvariable=self.registerUserSurname,
                                          font=("Helvetica", 13))
        self.registerInputSurname.grid(row=1, column=1, padx=5, pady=5)

        self.registerLogin = Label(self.registerFrame, text="Email: ", font=("Helvetica", 13))
        self.registerLogin.grid(row=2, column=0, sticky="wn", padx=5, pady=5)

        self.registerInputLogin = Entry(self.registerFrame, textvariable=self.registerUserLogin, font=("Helvetica", 13))
        self.registerInputLogin.grid(row=2, column=1, padx=5, pady=5)

        self.registerPassword = Label(self.registerFrame, text="Пароль: ", font=("Helvetica", 13))
        self.registerPassword.grid(row=3, column=0, sticky="wn", padx=5, pady=5)

        self.registerInputPassword = Entry(self.registerFrame, textvariable=self.registerUserPassword, show="*",
                                           font=("Helvetica", 13))
        self.registerInputPassword.grid(row=3, column=1, padx=5, pady=5)

        self.registerTwoPassword = Label(self.registerFrame, text="Повторний пароль: ", font=("Helvetica", 13))
        self.registerTwoPassword.grid(row=4, column=0, sticky="wn", padx=5, pady=5)

        self.registerInputTwoPassword = Entry(self.registerFrame, textvariable=self.registerUserTwoPassword, show="*",
                                              font=("Helvetica", 13))
        self.registerInputTwoPassword.grid(row=4, column=1, padx=5, pady=5)

        self.registerCancel = Button(self.registerFrame, width=15, height=2, text="Скасувати", font=("Helvetica", 13),
                                     command=lambda: self.clickCancel())
        self.registerCancel.grid(row=5, column=0, padx=5, pady=5)

        self.registerConfirm = Button(self.registerFrame, width=15, height=2, text="Підтвердити",
                                      font=("Helvetica", 13),
                                      command=lambda: self.clickConfirmRegister())
        self.registerConfirm.grid(row=5, column=1, padx=5, pady=5)

    # ==============================================
    # функція яка виконується при написканні клавіні log_in
    # ==============================================
    def clickLogin(self, *args):
        global userID, url

        login_data = {
            'email': self.userLogin.get().replace(' ', ''),
            'password': self.userPassword.get()
        }

        if login_data["email"] != '' and login_data['password'] != '':

            if self.checkEmail(login_data['email']) == 1:

                r = requests.get(url=url + '/checkEmailAndPassword', json=login_data)
                if r.ok:
                    r.json()
                # print(r.text)
                if int(r.text) == 1:

                    r = requests.get(url=url + '/selectID', json=login_data)

                    if r.ok:
                        r.json()
                    # print('ID адрес корисувача: ' + str(r.text))

                    userID = int(r.text)

                    self.loginFrame.grid_forget()

                    add = PersonalOffice(self.master)
                    add.updateListElement()
                else:
                    messagebox.showinfo('Повідомлення', 'Такий логін і пароль не зареєстрований.')
            else:
                messagebox.showinfo('Повідомлення', 'Некоректний логін, спробуйте щераз.')
        else:
            messagebox.showinfo('Повідомлення', 'Поля логіну і паролю не можуть бути порожніми.')

    # ==============================================
    # функція яка виконується при написканні клавіні register
    # ==============================================
    def clickRegister(self, *args):
        self.master.title("Реєстрація")

        self.inputPassword.delete(0, END)
        self.inputLogin.delete(0, END)

        self.loginFrame.grid_forget()

        self.registerFrame.grid(padx=80, pady=30)

    # ==============================================
    # функція яка виконується при написканні клавіні confirm
    # ==============================================
    def clickConfirmRegister(self, *args):
        global name, surname, email, password, twoPassword

        self.master.title("Авторизація")

        name = self.registerUserName.get().replace(' ', '')
        surname = self.registerUserSurname.get().replace(' ', '')
        email = self.registerUserLogin.get().replace(' ', '')
        password = self.registerUserPassword.get()
        twoPassword = self.registerUserTwoPassword.get()

        # ПЕРЕВІРКА НА ЗАПОВНЕНІ ПОЛЯ
        if name != '' and surname != '' and email != '' and password != '' and twoPassword != '':

            # ПЕРЕВІРКА НА КОРЕКТНО ВВЕДЕНИЙ ЛОГІН
            if (self.checkEmail(email)) == 1:

                # ПЕРЕВІРКА НА ЗБІЗАННЯ ПАРОЛІВ
                if password == twoPassword:

                    email_new_user = {
                        'email': email
                    }

                    r = requests.get(url=url + '/checkUserEmail', json=email_new_user)

                    if r.ok:
                        r.json()
                        # print(r.json())

                    # ПЕРЕВІРКА НА ПРИМУТНІТЬ ЛОГІНА У БАЗІ ДАНИХ
                    if int(r.text) == 0:

                        register_data = {
                            'name': name,
                            'surname': surname,
                            'email': email,
                            'password': password
                        }

                        r = requests.get(url=url + '/addNewUser', json=register_data)
                        if r.ok:
                            r.json()
                            # print(r.json())

                        self.cleanEntry()

                        self.registerFrame.grid_forget()

                        self.loginFrame.grid(padx=100, pady=50)
                    else:
                        messagebox.showinfo('Повідомлення', 'Користувач з таким логіном уже зарейстрований.')
                else:
                    messagebox.showinfo('Повідомлення', 'Паролі не збігаються, введіть однакові паролі.')
            else:
                messagebox.showinfo('Повідомлення', 'Некоректно ведений логін, спробуйте щераз.')
        else:
            messagebox.showinfo('Повідомлення', 'Усі поля повинні бути заповненні.')

    # ==============================================
    # функція яка виконується при написканні клавіні registerCancel
    # ==============================================
    def clickCancel(self, *args):
        self.master.title("Авторизація")

        self.cleanEntry()

        self.registerFrame.grid_forget()

        self.loginFrame.grid(padx=100, pady=50)

    # ==============================================
    # перевірка на правльність написаня email
    # ==============================================
    def checkEmail(self, email):
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if re.search(regex, email):
            return 1
        else:
            return 0

    def cleanEntry(self, *args):
        self.registerInputName.delete(0, END)
        self.registerInputSurname.delete(0, END)
        self.registerInputLogin.delete(0, END)
        self.registerInputPassword.delete(0, END)
        self.registerInputTwoPassword.delete(0, END)


class PersonalOffice:
    message: str

    def __init__(self, master):
        self.master = master

        self.master.winfo_toplevel().title("Особистик кабінет")
        self.master.winfo_toplevel().geometry('500x300')

        center_window(self.master, 450, 360)
        # ==============================================
        # основні фрейми класу personalOffice
        # ==============================================
        self.personalOfficeFrame = Frame(master)
        self.personalOfficeFrame.grid(padx=60)
        # ==============================================
        # два фрейма на який поділяється основний
        # ==============================================
        self.listButtonFrame = Frame(self.personalOfficeFrame)
        self.listButtonFrame.grid(row=0, column=0, sticky="n")

        self.listElementFrame = Frame(self.personalOfficeFrame)
        self.listElementFrame.grid(row=0, column=1)
        # ==============================================
        # фрейм для редагування особистої інформації
        # ==============================================
        self.editUserDataFrame = Frame(master)
        # ==============================================
        # фрейм для додавання нового елемента
        # ==============================================
        self.addNewElementFrame = Frame(master)

        self.nameNewElement = StringVar()
        # ==============================================
        # змінні для редагування особистої інформації
        # ==============================================
        self.variableEditUserName = StringVar()
        self.variableEditUserSurname = StringVar()
        self.variableEditUserLogin = StringVar()
        self.variableEditUserPassword = StringVar()
        self.variableEditUserTwoPassword = StringVar()
        # ==============================================
        # основні кнопки на особистому кабінеті, які знаходяться у фреймі personalOfficeFrame
        # ==============================================
        self.buttonAddElement = Button(self.listButtonFrame, width=40, height=2, text='Додати новий елемент',
                                       font=("Helvetica", 13),
                                       command=lambda: self.clickAddElement())
        self.buttonAddElement.grid(row=0, column=0)

        self.buttonUpdateListElement = Button(self.listButtonFrame, width=40, height=2, text='Оновити список елементів',
                                              font=("Helvetica", 13),
                                              command=lambda: self.updateListElement())
        self.buttonUpdateListElement.grid(row=1, column=0)

        self.buttonEditUserDate = Button(self.listButtonFrame, width=40, height=2,
                                         text='Редагувати особисту інформацію', font=("Helvetica", 13),
                                         command=lambda: self.clickEditUserDate())
        self.buttonEditUserDate.grid(row=2, column=0)

        self.buttonExit = Button(self.listButtonFrame, width=40, height=2, text='Вихід', font=("Helvetica", 13),
                                 command=lambda: self.clickExit())
        self.buttonExit.grid(row=3, column=0)

        # ==============================================
        #  вже що входить у фрейм для редагування особистої інформації
        # ==============================================
        self.editUserName = Label(self.editUserDataFrame, text="Ім'я: ", font=("Helvetica", 13))
        self.editUserName.grid(row=0, column=0, sticky="wn", padx=5, pady=5)

        self.editUserInputName = Entry(self.editUserDataFrame, textvariable=self.variableEditUserName,
                                       font=("Helvetica", 13))
        self.editUserInputName.grid(row=0, column=1, padx=5, pady=5)

        self.editUserSurname = Label(self.editUserDataFrame, text="Прізвище: ", font=("Helvetica", 13))
        self.editUserSurname.grid(row=1, column=0, sticky="wn", padx=5, pady=5)

        self.editUserInputSurname = Entry(self.editUserDataFrame, textvariable=self.variableEditUserSurname,
                                          font=("Helvetica", 13))
        self.editUserInputSurname.grid(row=1, column=1, padx=5, pady=5)

        self.editUserLogin = Label(self.editUserDataFrame, text="Ваш Email: ", font=("Helvetica", 13))
        self.editUserLogin.grid(row=2, column=0, sticky="wn", padx=5, pady=5)

        self.editUserInputLogin = Entry(self.editUserDataFrame, textvariable=self.variableEditUserLogin,
                                        font=("Helvetica", 13))
        self.editUserInputLogin.grid(row=2, column=1, padx=5, pady=5)

        self.editUserPassword = Label(self.editUserDataFrame, text="Пароль: ", font=("Helvetica", 13))
        self.editUserPassword.grid(row=3, column=0, sticky="wn", padx=5, pady=5)

        self.editUserInputPassword = Entry(self.editUserDataFrame, textvariable=self.variableEditUserPassword, show="*",
                                           font=("Helvetica", 13))
        self.editUserInputPassword.grid(row=3, column=1, padx=5, pady=5)

        self.editUserTwoPassword = Label(self.editUserDataFrame, text="Повторіть пароль: ", font=("Helvetica", 13))
        self.editUserTwoPassword.grid(row=4, column=0, sticky="wn", padx=5, pady=5)

        self.editUserInputTwoPassword = Entry(self.editUserDataFrame, textvariable=self.variableEditUserTwoPassword,
                                              show="*", font=("Helvetica", 13))
        self.editUserInputTwoPassword.grid(row=4, column=1, padx=5, pady=5)
        # ==============================================
        # кнопки скасувати і підтвердити редагування особистої інформації
        # ==============================================
        self.editConfirmUserData = Button(self.editUserDataFrame, width=15, height=2, text="Редагувати",
                                          font=("Helvetica", 13),
                                          command=lambda: self.clickConfirmEditUserData())
        self.editConfirmUserData.grid(row=5, column=1, padx=5, pady=5)
        self.cancelEditUserData = Button(self.editUserDataFrame, width=15, height=2, text="Скасувати",
                                         font=("Helvetica", 13),
                                         command=lambda: self.clickCancelEditUserData())
        self.cancelEditUserData.grid(row=5, column=0, padx=5, pady=5)
        # ==============================================
        # все що входить у фрейм для довання нового елемента списуку addNewElementFrame
        # ==============================================
        self.newElementName = Label(self.addNewElementFrame, text='Назва елемента: ', font=("Helvetica", 13))
        self.newElementName.grid(row=0, column=0, padx=5, pady=5)

        self.newElementInputName = Entry(self.addNewElementFrame, textvariable=self.nameNewElement,
                                         font=("Helvetica", 13))
        self.newElementInputName.grid(row=0, column=1, padx=5, pady=5)

        self.cancelAddNewElement = Button(self.addNewElementFrame, width=15, height=2, text='Скасувати',
                                          font=("Helvetica", 13),
                                          command=lambda: self.clickCancelAddNewElement())
        self.cancelAddNewElement.grid(row=1, column=0, padx=5, pady=5)

        self.confirmAddNewElement = Button(self.addNewElementFrame, width=15, height=2, text='Додати',
                                           font=("Helvetica", 13),
                                           command=lambda: self.clickConfirmAddNewElement())
        self.confirmAddNewElement.grid(row=1, column=1, padx=5, pady=5)
        # ==============================================
        # ==============================================
        # ==============================================
        Label(self.personalOfficeFrame, text='Список елементів', font=("Helvetica", 13)).grid(row=1, column=0)

        self.listElementFrame = Frame(self.personalOfficeFrame, width=310, height=170)
        self.listElementFrame.grid(row=2, column=0)

        self.canvas = Canvas(self.listElementFrame, bg='white', width=310, height=170)
        self.scroll_y = Scrollbar(self.listElementFrame, orient="vertical", command=self.canvas.yview)

        self.activeListElementFrame = Frame(self.canvas, bg='white', width=310, height=120)
        self.activeListElementFrame.pack(expand=True, fill='y', side='top')

    # ==============================================
    # функція для видалення одного елемента зі списку
    # ==============================================
    def deleteOneElementList(self, id_element, name_element):
        global number_list

        message = messagebox.askokcancel(message="Ви дійсно хочете видалити цей елемент ?")

        if message == True:

            data_element = {
                'id_element': id_element,
                'name_element': name_element
            }

            r = requests.get(url=url + '/deleteOneElement', json=data_element)
            if r.ok:
                r.json()
                # print(r.json())

            self.activeListElementFrame.destroy()
            self.updateListElement()
            number_list -= 1

    def clickOnOffElement(self, id_element, name_element):

        data_element = {
            'id_element': id_element,
            'name_element': name_element
        }

        r = requests.get(url=url + '/updateOnOffElement', json=data_element)
        if r.ok:
            r.json()
        # print("Element: " + r.text)

        self.activeListElementFrame.destroy()
        self.updateListElement()

    # ==============================================
    # функція для оновлення списку елементів
    # ==============================================
    def updateListElement(self):
        global number_list, userID
        number_list = 0

        self.listElementFrame = Frame(self.personalOfficeFrame, width=320)
        self.listElementFrame.grid(row=2, column=0)

        self.canvas = Canvas(self.listElementFrame, bg='white', width=320)
        self.scroll_y = Scrollbar(self.listElementFrame, orient="vertical",
                                  command=self.canvas.yview)

        self.activeListElementFrame = Frame(self.canvas, bg='white', width=320)
        self.activeListElementFrame.pack(expand=True, fill='y', side='top')

        id_element = {
            'id': userID
        }

        r = requests.get(url=url + '/selectNameElement', json=id_element)
        if r.ok:
            r.json()

        data = r.json()

        button_delete = []
        button_on_off = []

        count = 0

        for activeName in data:
            activeName[0].replace(' ', '')

            oneElementFrame = Frame(self.activeListElementFrame, highlightbackground="black", highlightcolor="black",
                                    highlightthickness=2, bd=1)
            oneElementFrame.pack(side=TOP, padx=3, pady=3)

            Label(oneElementFrame, width=20, height=2, text=activeName[0]).grid(row=0, column=0, padx=3, pady=3)

            button_on_off.append(Button(oneElementFrame, width=5, height=2, text=activeName[1],
                                        command=lambda id_element=userID, name_element=activeName[0]:
                                        self.clickOnOffElement(id_element, name_element)))
            button_on_off[count].grid(row=0, column=1, padx=3, pady=3)

            button_delete.append(Button(oneElementFrame, width=5, height=2, text='Delete',
                                        command=lambda id_element=userID, name_element=activeName[0]:
                                        self.deleteOneElementList(id_element, name_element)))
            button_delete[count].grid(row=0, column=2, padx=3, pady=3)
            count += 1
            number_list += 1

        # put the frame in the canvas
        self.canvas.create_window(0, 0, anchor='nw', window=self.activeListElementFrame, tags="frame")
        # make sure everything is displayed before configuring the scrollregion
        self.canvas.update_idletasks()

        self.canvas.pack(fill='both', expand=True, side='left')

        self.canvas.bind("<Configure>", lambda e: self.canvas.configure(
            scrollregion=self.canvas.bbox("all"), yscrollcommand=self.scroll_y.set))

        self.scroll_y.pack(fill='y', side='right')
        self.canvas.yview_moveto(0)

    # ==============================================
    # функція при натискані кнопки яка відкриває фреейм для редагування особистих даних
    # ==============================================
    def clickEditUserDate(self, *args):
        global name, surname, email, password, twoPassword, userID
        list_register_data = {}
        user_data = {
            'user_id': userID
        }
        r = requests.get(url=url + '/selectUserData', json=user_data)
        if r.ok:
            list_register_data = r.json()

        self.master.title('Редагування особистих даних')

        self.editUserInputName.insert(0, list_register_data['name'])
        self.editUserInputSurname.insert(0, list_register_data['surname'])
        self.editUserInputLogin.insert(0, list_register_data['email'])
        self.editUserInputPassword.insert(0, list_register_data['password'])
        self.editUserInputTwoPassword.insert(0, list_register_data['password'])

        self.personalOfficeFrame.grid_forget()
        self.editUserDataFrame.grid(padx=60, pady=60)

    # ==============================================
    # функція при натискані кнопки яка відміняє редагування особистих даних користувача
    # ==============================================
    def clickCancelEditUserData(self, *args):
        self.master.title('Особистий кабінет')

        self.cleanEntry()

        self.editUserDataFrame.grid_forget()
        self.personalOfficeFrame.grid(padx=60)

    # ==============================================
    # функція при натискані кнопки для підведження зміни особистих даних користувача
    # ==============================================
    def clickConfirmEditUserData(self, *args):
        global name, surname, email, password, twoPassword, userID

        self.master.title('Особистий кабінет')

        name = self.variableEditUserName.get().replace(' ', '')
        surname = self.variableEditUserSurname.get().replace(' ', '')
        email = self.variableEditUserLogin.get().replace(' ', '')
        password = self.variableEditUserPassword.get()
        twoPassword = self.variableEditUserTwoPassword.get()

        check = LoginRegister(self.master)
        check.loginFrame.grid_forget()

        if check.checkEmail(email) == 1:
            if twoPassword == password:

                user_data = {
                    'user_name': name,
                    'user_surname': surname,
                    'user_email': email,
                    'user_password': password,
                    'user_id': userID
                }
                r = requests.get(url=url + '/editUserData', json=user_data)
                if r.ok:
                    r.json()
                # print(r.text)

                self.cleanEntry()

                self.editUserDataFrame.grid_forget()
                self.personalOfficeFrame.grid(padx=60)
            else:
                messagebox.showinfo('Повідомлення', 'Паролі не збігаються, введіть однакові паролі.')
        else:
            messagebox.showinfo('Повідомлення', 'Некоректно ведений логін, спробуйте щераз.')

    # ==============================================
    # функція для додавання нових елементів до списку при натискані кнопки
    # ==============================================
    def clickAddElement(self, *args):
        global number_list

        if number_list < 10:
            self.master.title('Додати новий елемент')

            self.personalOfficeFrame.grid_forget()
            self.addNewElementFrame.grid(padx=60, pady=125)
            number_list += 1
        else:
            messagebox.showinfo('Повідомлення', 'Список елементів переповнений.')

    # ==============================================
    # функція при якій скасовуємо додавання нового елемента  при натискані кнопки
    # ==============================================
    def clickCancelAddNewElement(self, *args):
        self.master.title('Особистий кабінет')

        self.newElementInputName.delete(0, END)

        self.addNewElementFrame.grid_forget()
        self.personalOfficeFrame.grid(padx=60)

    # ==============================================
    # функція при якій додоється новий елемент у канвас при натискані кнопки
    # ==============================================

    def clickConfirmAddNewElement(self, *args):

        data_element = {
            'id_element': userID,
            'element_name': self.newElementInputName.get(),
            'on_off_element': 'OFF'
        }

        r = requests.get(url=url + '/addNewElement', json=data_element)
        if r.ok:
            r.json()

        if int(r.text) == 1:
            self.newElementInputName.delete(0, END)

            self.activeListElementFrame.destroy()

            self.addNewElementFrame.grid_forget()
            self.personalOfficeFrame.grid(padx=60)

            self.updateListElement()
        else:
            messagebox.showinfo('Повідомлення', "Елемент з таким іменем уже присутній у списку, змініть ім'я елементу")

    # ==============================================
    # функція для виходу з особистого кабінету при натискані кнопки
    # ==============================================
    def clickExit(self, *args):
        self.message = messagebox.askokcancel(message="Ви дійсно хочете вийти з акаунта?")

        if self.message == 1:
            self.personalOfficeFrame.grid_forget()
            LoginRegister(self.master)

    # ==============================================
    # функція для очищення Entry щоб повторно не заносилася інформація
    # ==============================================
    def cleanEntry(self, *args):
        self.editUserInputName.delete(0, END)
        self.editUserInputSurname.delete(0, END)
        self.editUserInputLogin.delete(0, END)
        self.editUserInputPassword.delete(0, END)
        self.editUserInputTwoPassword.delete(0, END)


def center_window(window, w, h):
    # get screen width and height
    ws = window.winfo_screenwidth()
    hs = window.winfo_screenheight()
    # calculate position x, y
    x = (ws / 2) - (w / 2)
    y = (hs / 2) - (h / 2)
    window.geometry('%dx%d+%d+%d' % (w, h, x, y))


def main():

    ConnectToTheDatabase(root)
    # LoginRegister(root)
    # PersonalOffice(root)

    root.mainloop()


if __name__ == '__main__':
    main()
