from flask import Flask, request, jsonify
from flaskext.mysql import MySQL

main_ip = '192.168.1.101'

mysql = MySQL()

app = Flask(__name__)

app.config['MYSQL_DATABASE_USER'] = 'user'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'my_data_base'
app.config['MYSQL_DATABASE_HOST'] = main_ip
mysql.init_app(app)


conn = mysql.connect()
cursor = conn.cursor()


cursor.execute("DELETE FROM listElement WHERE idElement = 10")
conn.commit()


@app.route('/connectServer', methods=['GET', 'POST'])
def connectServer():
    ip_server = request.get_json()

    if str(ip_server['ip_server']) == main_ip:
        return jsonify(1)
    else:
        return jsonify(0)


@app.route('/selectID', methods=['GET', 'POST'])
def selectID():
    login_data = request.get_json()

    cursor.execute("SELECT id from MyUsers WHERE email = %s AND password = %s",
                   (login_data['email'],
                    login_data['password']))

    data = cursor.fetchone()
    print(str(data[0]))
    return jsonify(data[0])


@app.route('/checkEmailAndPassword', methods=['GET', 'POST'])
def checkEmailAndPassword():
    login_data = request.get_json()

    cursor.execute("SELECT email, password FROM MyUsers WHERE email = %s AND password = %s",
                   (login_data['email'],
                    login_data['password']))

    if cursor.rowcount == 1:
        return jsonify(1)
    else:
        return jsonify(0)


@app.route('/addNewUser', methods=['GET', 'POST'])
def addNewUser():
    register_data = request.get_json()
    print(register_data)

    cursor.execute("INSERT INTO MyUsers(name, surname, email, password) VALUES (%s, %s, %s, %s)",
                   (register_data['name'],
                    register_data['surname'],
                    register_data['email'],
                    register_data['password']))
    conn.commit()
    return jsonify('Register True')


@app.route('/checkUserEmail', methods=['GET', 'POST'])
def checkUserEmail():
    email_new_user = request.get_json()

    cursor.execute("SELECT email FROM MyUsers WHERE email = %s", email_new_user['email'])
    if cursor.rowcount == 0:
        return jsonify(0)
    else:
        return jsonify(1)


@app.route('/deleteOneElement', methods=['GET', 'POST'])
def deleteOneElement():
    data_element = request.get_json()

    cursor.execute("DELETE FROM listElement WHERE idElement = %s AND nameElement = %s",
                   (data_element['id_element'],
                    data_element['name_element']))
    conn.commit()

    return jsonify('Delete True')


@app.route('/selectNameElement', methods=['GET', 'POST'])
def selectNameElement():
    id_element = request.get_json()

    cursor.execute("SELECT idElement FROM listElement WHERE idElement = %s",
                   id_element['id'])

    number = cursor.rowcount
    data = []

    cursor.execute("SELECT nameElement, ON_OFF FROM listElement WHERE idElement = %s",
                   id_element['id'])

    for count in range(number):
        a = cursor.fetchone()
        data.append([a[0], a[1]])

    return jsonify(data)


@app.route('/updateOnOffElement', methods=['GET', 'POST'])
def updataOnOffElement():
    data_element = request.get_json()

    cursor.execute("SELECT ON_OFF FROM listElement WHERE idElement = %s AND nameElement = %s",
                   (data_element['id_element'],
                    data_element['name_element']))
    data = cursor.fetchone()

    if data[0] == 'OFF':
        on_off_element_0 = 'ON'

        cursor.execute("UPDATE ListElement SET ON_OFF = %s WHERE idElement = %s AND nameElement = %s",
                       (on_off_element_0,
                        data_element['id_element'],
                        data_element['name_element']))
        conn.commit()

        return jsonify(on_off_element_0)
    else:
        on_off_element_1 = "OFF"

        cursor.execute("UPDATE ListElement SET ON_OFF = %s WHERE idElement = %s AND nameElement = %s",
                       (on_off_element_1,
                        data_element['id_element'],
                        data_element['name_element']))
        conn.commit()

        return jsonify(on_off_element_1)


@app.route('/editUserData', methods=['GET', 'POST'])
def editUserData():
    user_data = request.get_json()

    cursor.execute("UPDATE MyUsers SET name = %s, surname = %s, email = %s, password = %s WHERE id = %s",
                   (user_data['user_name'],
                    user_data['user_surname'],
                    user_data['user_email'],
                    user_data['user_password'],
                    user_data['user_id']))
    conn.commit()
    return jsonify('Edit Data True')


@app.route('/selectUserData', methods=['GET', 'POST'])
def selectUserData():
    user_data = request.get_json()

    cursor.execute("SELECT name, surname, email, password from MyUsers WHERE id = %s",
                   user_data['user_id'])
    a = cursor.fetchone()

    register_data = {
        'name': a[0],
        'surname': a[1],
        'email': a[2],
        'password': a[3]
    }

    return jsonify(register_data)


@app.route('/addNewElement', methods=['GET', 'POST'])
def addNewElement():
    data_element = request.get_json()

    cursor.execute("SELECT nameElement FROM ListElement WHERE idElement = %s AND nameElement = %s",
                   (data_element['id_element'],
                    data_element['element_name']))

    if cursor.rowcount == 0:

        cursor.execute("INSERT INTO ListElement(idElement, nameElement, ON_OFF) VALUES (%s, %s, %s)",
                       (data_element['id_element'],
                        data_element['element_name'],
                        data_element['on_off_element']))
        conn.commit()
        return jsonify(1)
    else:
        return jsonify(0)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')
